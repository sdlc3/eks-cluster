# Project Infrastructure #

This project infrastructure will create the following things

* VPC
* Internet Gateway
* Public and Private Subnets in 3 AZ
* Route tables and associations
* Elastic IP
* NAT
* EKS Cluster
* Spot instance Node Group
* Control plane and Node security groups
* EKS Cluster Roles
* Policies to ELB, CLoudwatch, EKS, etc
* Cluster Autoscaling Group
* EFS Provisioner
* EFS Storage Class
* EFS PVC for MYSQL

Pods will be launched into Private Subnet. Services as Load balancers(ELB) will be launched in Public Subnet. MySQL service is running as headless service for security.
### Pipeline ###

This repo is configured in the following Jenkins instance.

[Jenkins](http://54.255.205.125:8080/)

Following multi branch pipeline is configured

[Pipeline](http://54.255.205.125:8080/job/eks-cluster/)

Jenkinsfile calls the central jenkins shared library.

[Jenkins-Shared-Library](https://sivakmr469@bitbucket.org/sdlc3/jenkins-pipelines.git)